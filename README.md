Start VM in various cloud providers
===================================

This role starts VMs in various cloud providers listed in vars/mail.yml:cloud_providers

Requirements
------------
 - boto3 package is required because this role uses ec2_instance module

Role Variables
--------------

For now all variables are extracted from the YAML inventory file

Dependencies
------------

If you're using AWS profiles you will need and extra role that does *sts_assume_role*.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Create gitlab_runners machines in AWS
      gather_facts: False
      hosts: gitlab_runners
      roles:
        - virtual_machine

The above can be ran with the following inventory:

    ---
    all:
      children:
        gitlab_runners:
          hosts:
            gitlab-runner-[01:02].example.com:
              provider: aws
              add_in_dns: true
              instance_type: t3a.medium
              image: centos7 # it will translate to ami id
              region: eu-west-1
              volumes:
                - device_name: /dev/sda1
                  ebs:
                    volume_type: gp2
                    volume_size: 80
                    delete_on_termination: yes
              group:
                - "Some First Group"
                - "Some Other Group"
              vpc_subnet: subnet-aa11ee88
              tags:
                Env: prod
                Role: gitlab-runner
